#include <ros/ros.h>
#include <ros/package.h>
#include <std_msgs/Empty.h>

#include <utility>
#include <cell_controller.h>

#include "Scene.pb.h"

#include "ccf/controller/DummyRobotArmController.h"
#include "ccf/connection/NngConnection.h"
#include "ccf/connection/MqttConnection.h"
#include "ccf/util/NodeUtil.h"

const char* DEFAULT_NODE_NAME = "main_controller";
std::string NODE_NAME;

using CetiRosToolbox::getParameter;
using CetiRosToolbox::getPrivateParameter;

const float UNSELECT = 0.8;
const float SELECT = 1 / UNSELECT;
const float DELETING = 0.08;

void highlight(Object* selected_bin, float factor)
{
  if (selected_bin)
  {
    selected_bin->mutable_color()->set_r(selected_bin->color().r() * factor);
    selected_bin->mutable_color()->set_g(selected_bin->color().g() * factor);
    selected_bin->mutable_color()->set_b(selected_bin->color().b() * factor);
  }
}

int main(int argc, char** argv)
{
  GOOGLE_PROTOBUF_VERIFY_VERSION;

  ros::init(argc, argv, DEFAULT_NODE_NAME);
  NODE_NAME = ros::this_node::getName();

  ros::NodeHandle n("connector_node_ros_ccf");  // namespace where the connection_address is

  auto connection_address = getPrivateParameter<std::string>("connection_address", "tcp://*:6576");

  auto robotName = getPrivateParameter<std::string>("arm", "virtual-arm");
  ROS_INFO_STREAM("This cell controls arm " << robotName);

  DummyRobotArmController controller{ n, NODE_NAME, robotName };

  std::map<std::string, CellController> clients;
  std::vector<std::pair<std::string, std::string>> actions;

  // add an NNG connection
  std::unique_ptr<NngConnection> connection = std::make_unique<NngConnection>(connection_address);
  connection->setReceiveTopic(getParameter<std::string>(n, "topics/selection", "selection"));
  connection->setSendTopic(getParameter(n, "topics/scene", NODE_NAME + "/scene/update"));
  controller.addConnection(std::move(connection));

  auto client_controllers = getPrivateParameter<std::vector<std::string>>(
      "client_controllers", { "ceti_cell_1", "ceti_cell_2", "ads-cell", "st-cell" });
  auto mqtt_server = getPrivateParameter<std::string>("mqtt_server", "tcp://127.0.0.1:1883");

  // create an empty task
  auto task = std::make_shared<Task>();

  ROS_INFO_STREAM("Connecting to " << client_controllers.size() << " client controllers.");
  std::shared_ptr<MqttConnection> client_connection = std::make_shared<MqttConnection>(mqtt_server, NODE_NAME);
  controller.addConnection(client_connection);
  for (const auto& client : client_controllers)
  {
    ROS_INFO_STREAM("Connecting to client at " << client << ".");
    clients[client].setCellId(client);
    clients[client].setCurrentTask(task);
    clients[client].setActionCallback([client, &controller, &clients](const Action& action) {
      ROS_INFO_STREAM("Action Callback Called for " << action);
      Command command;
      command.mutable_pickandplace()->set_idrobot(clients[client].cell().robot());
      command.mutable_pickandplace()->set_idpick(action.getSource());
      command.mutable_pickandplace()->set_idplace(action.getTarget());
      controller.sendToAll("/" + client + "/command", command.SerializeAsString());
    });
    auto scene_update_topic = "/" + client + getParameter<std::string>(n, "topics/scene", "/scene/update");
    ROS_INFO_STREAM("Listening to MQTT topic " << scene_update_topic);
    client_connection->listen(scene_update_topic);
    client_connection->listen(getParameter<std::string>(n, "topics/selection", "selection"));
    controller.addCallback(scene_update_topic, [client, &clients](auto msg) {
      ROS_INFO_STREAM("Got an updated scene from client " << client);
      clients[client].updateModel(msg);
      // uncomment in case things fail. clients[client].proceedWithTask();
    });
  }
  auto fallback_path =
      ros::package::getPath("ccf_immersive_sorting") + "/config/config_scene_virtual-tag-table-grey.json";
  controller.loadScene(getPrivateParameter<std::string>("scene", fallback_path));

  Object* robot = controller.resolveObject(controller.getRobotName());
  Object* selected_box = nullptr;
  Object* selected_bin = nullptr;
  std::optional<std::string> picked_box{};

  ros::Subscriber sub = n.subscribe<std_msgs::Empty>(
      "send_scene", 1000, [&controller](const std_msgs::EmptyConstPtr& msg) { controller.sendScene(); });
  ros::Timer timer = n.createTimer(ros::Duration(10), [&controller](const ros::TimerEvent& event) {
    controller.sendScene();
  });  // send a scene every ten seconds

  //
  ros::Timer timer_scene_log = n.createTimer(ros::Duration(10), [&clients](const ros::TimerEvent& event) {
    for (const auto& [name, cell_controller] : clients)
    {
      ROS_WARN_STREAM("CLIENT " << name << " has a cell controller with " << cell_controller.cell().scene.objects_size()
                                << " objects which is " << cell_controller.cell().stateString());
    }
  });

  auto selection_message_callback = [&picked_box, &controller, &robot, &selected_box, &selected_bin, &task,
                                     &clients](const Selection& selection) {
    if (picked_box.has_value())
    {
      ROS_WARN_STREAM("Unable to accept selections while picking is in progress.");
      return;
    }

    Object* object = controller.resolveObject(selection.id());
    if (!object)
    {
      ROS_ERROR_STREAM("Selected unknown object '" << selection.id() << "'");
      return;
    }
    auto type = Object::Type_Name(object->type());
    if (object->type() == Object::BOX)
    {
      highlight(selected_box, UNSELECT);
      if (selected_box == object)
      {
        selected_box = nullptr;
      }
      else
      {
        selected_box = object;
        highlight(selected_box, SELECT);
      }
      controller.sendScene();
      ROS_INFO_STREAM("Selected " << type << " '" << selection.id() << "'");
    }
    else if (object->type() == Object::BIN)
    {
      highlight(selected_bin, UNSELECT);
      if (selected_bin == object)
      {
        selected_bin = nullptr;
      }
      else
      {
        selected_bin = object;
        highlight(selected_bin, SELECT);
      }
      controller.sendScene();
      ROS_INFO_STREAM("Selected " << type << " '" << selection.id() << "'");
    }
    else
    {
      ROS_WARN_STREAM("Selected unsupported " << type << " '" << selection.id() << "'");
    }

    if (selected_bin && selected_box)
    {
      auto boxId = selected_box->id();
      ROS_INFO_STREAM("Got task: Put " << boxId << " into " << selected_bin->id());
      picked_box = boxId;
      highlight(selected_bin, UNSELECT);
      highlight(selected_box, DELETING);
      controller.sendScene();

      // create an action and add it to the current task
      Action pick_drop_action(Action::PICK_DROP, selected_box->id(), selected_bin->id());
      task->addAction(pick_drop_action);

      for (auto& [name, client] : clients)
      {
        client.proceedWithTask();
      }

      if (!controller.pickAndDrop(*robot, *selected_box, *selected_bin, false))
      {
        ROS_WARN_STREAM("Unable to remove box '" << boxId << "'!");
      }
      else
      {
        ROS_INFO_STREAM("Job is done! '" << boxId << "' is no more.");
      }
      selected_box = nullptr;
      selected_bin = nullptr;
      controller.sendScene();
    }
  };
  controller.reactToSelectionMessage(selection_message_callback);

  auto scene_update_callback = [&picked_box, &controller]() {
    if (picked_box.has_value())
    {
      auto resolved = controller.resolveObject(picked_box.value());
      if (!resolved)
      {
        ROS_INFO_STREAM("box " << picked_box.value() << " has been removed from the scene!");
        picked_box.reset();
      }
    }
  };
  controller.reactToSceneUpdateMessage(scene_update_callback);

  ros::spin();

  return 0;
}