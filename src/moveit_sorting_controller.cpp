/*! \file moveit_sorting_controller.cpp

    A ROS node that controls a robot connected to a model-based cobotic application

    \author Johannes Mey
    \author Sebastian Ebert
    \date 07.07.20
*/

#include <ros/ros.h>
#include <ros/package.h>
#include <std_msgs/Empty.h>

#include "Scene.pb.h"

#include "ccf/controller/MoveItRobotArmController.h"
#include "ccf/connection/NngConnection.h"
#include "ccf/connection/MqttConnection.h"
#include "ccf/util/NodeUtil.h"

std::string NODE_NAME = "ceti_cell_1";

using CetiRosToolbox::getParameter;
using CetiRosToolbox::getPrivateParameter;

int main(int argc, char** argv)
{
  GOOGLE_PROTOBUF_VERIFY_VERSION;

  ros::init(argc, argv, NODE_NAME);
  NODE_NAME = ros::this_node::getName();

  ros::NodeHandle n("connector_node_ros_ccf");  // namespace where the connection_address is

  auto robotName = getPrivateParameter<std::string>("arm", "arm");
  ROS_INFO_STREAM("This cell controls arm " << robotName);

  MoveItRobotArmController controller{ n, NODE_NAME, robotName };

  // we can only set an absolute workspace here, because we do not have the scene yet!
  geometry_msgs::Point minPoint, maxPoint;
  minPoint.x = -.58; // we need 58 free space, i.e. ~40 cm behind the table (if the robot is at -22)
  minPoint.y = -1;
  minPoint.z = 0; // the robot can not reach under itself, i.e., into the table
  maxPoint.x = 1;
  maxPoint.y = 1;
  maxPoint.z = 1.5;
  controller.setWorkspace(minPoint, maxPoint, true);

  auto mqttServer = getPrivateParameter<std::string>("mqtt_server", "localhost:1883");
  std::unique_ptr<MqttConnection> mqtt_connection =
      std::make_unique<MqttConnection>(mqttServer, ros::this_node::getName());
  mqtt_connection->listen(getParameter<std::string>(n, "topics/selection", "selection"));
  auto commandTopic = getParameter<std::string>(n, "topics/command", NODE_NAME + "/command");
  mqtt_connection->listen(commandTopic);
  auto other_cell_topic = getPrivateParameter<std::string>("other_cell", "/ceti_cell_2_placeworld/scene/delta-update");
  mqtt_connection->listen(other_cell_topic);
  auto scene_observer_topic = getPrivateParameter<std::string>("scene_observer", "object_locator/scene/update");
  mqtt_connection->listen(scene_observer_topic);
  controller.addConnection(std::move(mqtt_connection));

  controller.addCallback(other_cell_topic, [&controller](auto msg) {
    Scene scene;
    scene.ParseFromString(msg);
    ROS_INFO_STREAM("Updating scene from other cell. Is delta: " << scene.is_delta());
    controller.initScene(scene, false);
  });

  ros::WallDuration(5).sleep();  // wait 5 secs to init scene (to give moveit time to load)

  ros::Timer timer = n.createTimer(ros::Duration(3), [&controller](const ros::TimerEvent& event) {
    controller.sendScene();
  });  // send a scene every three seconds

  controller.loadScene(getPrivateParameter<std::string>("scene", ros::package::getPath("ccf_immersive_sorting") + "/config/config_scene_ceti-table-1-empty-grey.json"));

  // only add the scene observer after the base scene has been loaded
  controller.addCallback(scene_observer_topic, [&controller](auto msg) {
    ROS_INFO_STREAM("Updating scene from observer.");
    Scene scene;
    scene.ParseFromString(msg);
    controller.initScene(scene, true);
  });

  ros::Subscriber sub =
      n.subscribe<std_msgs::Empty>("send_scene", 1000, [&controller](const std_msgs::EmptyConstPtr& msg) {
        ROS_INFO_STREAM("Sending scene manually (triggered by a message to the 'send_scene' topic)");
        controller.sendScene();
      });

  auto pick_place_callback = [&controller](const Command& command) {
    if (command.has_pickandplace())
    {
      ROS_INFO_STREAM("[COMMAND] for robot " << command.pickandplace().idrobot() << ": Pick object "
                                             << command.pickandplace().idpick() << " and place it at "
                                             << command.pickandplace().idplace());
      Object* pick_object = controller.resolveObject(command.pickandplace().idpick());
      if (!pick_object)
      {
        ROS_ERROR_STREAM("[COMMAND] FAILED! Selected unknown pick object '" << command.pickandplace().idpick() << "'");
        return;
      }
      Object* place_object = controller.resolveObject(command.pickandplace().idplace());
      if (!place_object)
      {
        ROS_ERROR_STREAM("[COMMAND] FAILED! Selected unknown place object '" << command.pickandplace().idplace()
                                                                             << "'");
        return;
      }
      if (pick_object->type() != Object::BOX)
      {
        ROS_WARN_STREAM("[COMMAND] FAILED! Selected unsupported pick object of type " << pick_object->type());
      }
      if (place_object->type() != Object::BIN && place_object->type() != Object::DROP_OFF_LOCATION &&
          place_object->type() != Object::COLLABORATION_ZONE)
      {
        ROS_WARN_STREAM("[COMMAND] FAILED! Selected unsupported place object of type " << place_object->type());
      }
      if (command.pickandplace().idrobot() != controller.getRobotName())
      {
        ROS_DEBUG_STREAM("[COMMAND] IGNORED! Ignoring command pick-place command for robot "
                         << command.pickandplace().idrobot() << " (this is " << controller.getRobotName() << ")");
        return;
      }

      Object* robot = controller.resolveObject(command.pickandplace().idrobot());
      if (!robot)
      {
        ROS_ERROR_STREAM("[COMMAND] FAILED! Inconsistent scene, robot '" << command.evacuate().idrobot()
                                                                         << "' not found!");
        return;
      }
      if (place_object->type() == Object::BIN)
      {
        if (!controller.pickAndDrop(*robot, *pick_object, *place_object, false))
        {
          ROS_WARN_STREAM("[COMMAND] FAILED! Unable to remove box '" << command.pickandplace().idpick() << "'!");
        }
        else
        {
          ROS_INFO_STREAM("[COMMAND] SUCCESS! '" << command.pickandplace().idpick() << "' is no more.");
          controller.sendScene();
        }
      }
      else if (place_object->type() == Object::DROP_OFF_LOCATION || place_object->type() == Object::COLLABORATION_ZONE)
      {
        if (!controller.pickAndPlace(*robot, *pick_object, *place_object, false))
        {
          ROS_WARN_STREAM("[COMMAND] FAILED! Unable to move box '" << command.pickandplace().idpick() << "'!");
        }
        else
        {
          ROS_INFO_STREAM("[COMMAND] SUCCESS! " << command.pickandplace().idpick() << "' is at its new location.");
          controller.sendScene();
        }
      }
    }
    else if (command.has_evacuate())
    {
      ROS_INFO_STREAM("[COMMAND] for robot " << command.evacuate().idrobot() << ": Evacuate "
                                             << command.evacuate().idrobot() << " from zone "
                                             << command.evacuate().idcollaborationzone());
      if (command.evacuate().idrobot() != controller.getRobotName())
      {
        ROS_DEBUG_STREAM("[COMMAND] IGNORED! Ignoring evacuate command for robot "
                         << command.evacuate().idrobot() << " (this is " << controller.getRobotName() << ")");
        return;
      }

      Object* robot = controller.resolveObject(command.evacuate().idrobot());
      if (!robot)
      {
        ROS_ERROR_STREAM("[COMMAND] FAILED! Inconsistent scene, robot '" << command.evacuate().idrobot()
                                                                         << "' not found!");
        return;
      }

      geometry_msgs::Pose pose;
      pose.orientation.x = 1;
      pose.position.x = robot->pos().x() + .4;
      pose.position.y = robot->pos().y();
      pose.position.z = robot->pos().z() + .4;
      if (controller.moveToPose(*robot, pose, false))
      {
        ROS_INFO_STREAM("[COMMAND] SUCCESS! Evacuation complete!");
      }
      else
      {
        ROS_ERROR_STREAM("[COMMAND] FAILED! Evacuation did not complete.");
      }
    }
    else if (command.has_configchange())
    {
      Object* zone = controller.resolveObject(command.configchange().idcollaborationzone());
      ROS_INFO_STREAM("[COMMAND] Change config of zone " << zone->id() << " to owner "
                                                         << command.configchange().idrobotnewowner());
      if (!zone)
      {
        ROS_ERROR_STREAM("[COMMAND] FAILED! Unable to configure unknown collaboration zone '"
                         << command.evacuate().idrobot() << "'.");
        return;
      }

      Object* robot = controller.resolveObject(command.configchange().idrobotnewowner());
      if (!robot)
      {
        ROS_WARN_STREAM("[COMMAND] INFO: Collaboration zone active for unknown robot '" << command.evacuate().idrobot()
                                                                                        << "', so blocked in all.");
      }
      controller.configureCollaborationZone(*zone, command.configchange().idrobotnewowner());
    }
    else if (command.has_pick())
    {
      Object* robot = controller.resolveObject(command.pick().idrobot());
      Object* object = controller.resolveObject(command.pick().idpick());
      ROS_INFO_STREAM("[COMMAND] for robot " << command.pick().idrobot() << ": Pick " << object->id() << " using robot "
                                             << robot->id());
      if (!robot)
      {
        ROS_ERROR_STREAM("[COMMAND] FAILED! Unable to pick using unknown robot '" << command.pick().idrobot() << "'.");
        return;
      }
      if (!object)
      {
        ROS_ERROR_STREAM("[COMMAND] FAILED! Unable to pick unknown object '" << command.pick().idpick() << "'.");
        return;
      }
      controller.pick(command.pick().idrobot(), command.pick().idpick(), false);
    }
    else if (command.has_place())
    {
      Object* robot = controller.resolveObject(command.place().idrobot());
      Object* object = controller.resolveObject(command.place().idplace());
      ROS_INFO_STREAM("[COMMAND] for robot " << command.place().idrobot() << ": Place object at " << object->id()
                                             << " using robot " << robot->id());
      if (!robot)
      {
        ROS_ERROR_STREAM("[COMMAND] FAILED! Unable to place using unknown robot '" << command.place().idrobot()
                                                                                   << "'.");
        return;
      }
      if (!object)
      {
        ROS_ERROR_STREAM("[COMMAND] FAILED! Unable to place at unknown location '" << command.place().idplace()
                                                                                   << "'.");
        return;
      }
      controller.place(command.place().idrobot(), command.place().idplace(), false);
    }
    else if (command.has_drop())
    {
      Object* robot = controller.resolveObject(command.drop().idrobot());
      Object* bin = controller.resolveObject(command.drop().idbin());
      ROS_INFO_STREAM("[COMMAND] for robot " << command.drop().idrobot() << ":  drop object into bin " << bin->id()
                                             << " using robot " << robot->id());
      if (!robot)
      {
        ROS_ERROR_STREAM("[COMMAND] FAILED! Unable to drop using unknown robot '" << command.drop().idrobot() << "'.");
        return;
      }
      if (!bin)
      {
        ROS_ERROR_STREAM("[COMMAND] FAILED! Unable to drop into unknown bin '" << command.drop().idbin() << "'.");
        return;
      }
      controller.drop(command.drop().idrobot(), command.drop().idbin(), false);
    }
    else if (command.has_movetopose()){
      ROS_INFO_STREAM("[COMMAND] for robot " << command.movetopose().idrobot() << ": Move to position "
                                             << "x" << command.movetopose().position().x() << " "
                                             << "y" << command.movetopose().position().y() << " "
                                             << "z" << command.movetopose().position().z() << " "
                                             << " and orientation "
                                             << "x" << command.movetopose().orientation().x() << " "
                                             << "y" << command.movetopose().orientation().y() << " "
                                             << "z" << command.movetopose().orientation().z() << " "
                                             << "w" << command.movetopose().orientation().w());
      if (command.movetopose().idrobot() != controller.getRobotName())
      {
        ROS_DEBUG_STREAM("[COMMAND] IGNORED! Ignoring move-to-pose command for robot "
                         << command.movetopose().idrobot() << " (this is " << controller.getRobotName() << ")");
        return;
      }

      Object* robot = controller.resolveObject(command.movetopose().idrobot());
      if (!robot)
      {
        ROS_ERROR_STREAM("[COMMAND] FAILED! Inconsistent scene, robot '" << command.movetopose().idrobot()
                                                                         << "' not found!");
        return;
      }

      geometry_msgs::Pose pose;
      pose.orientation.x = command.movetopose().orientation().x();
      pose.orientation.y = command.movetopose().orientation().y();
      pose.orientation.z = command.movetopose().orientation().z();
      pose.orientation.w = command.movetopose().orientation().w();
      pose.position.x = command.movetopose().position().x();
      pose.position.y = command.movetopose().position().y();
      pose.position.z = command.movetopose().position().z();
      if (controller.moveToPose(*robot, pose, false))
      {
        ROS_INFO_STREAM("[COMMAND] SUCCESS! Move-to-pose complete!");
      }
      else
      {
        ROS_ERROR_STREAM("[COMMAND] FAILED! Move-to-pose did not complete.");
      }
    }
    else
    {
      ROS_WARN_STREAM("[COMMAND] IGNORED! Ignoring command of unknown type " << command.msg_case());
    }
  };
  controller.reactToCommandMessage(pick_place_callback);

  ros::spin();

  return 0;
}
