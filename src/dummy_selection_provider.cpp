/*! \file dummy_selection_provider.cpp

    A ROS node that simulates a selection provider like the CGV framework

    \author Johannes Mey
    \date 07.07.20
*/

#include "ros/ros.h"
#include "std_msgs/String.h"

#include "ccf/util/NodeUtil.h"

#include <nng/nng.h>
#include <nng/protocol/pair1/pair.h>

#include <google/protobuf/text_format.h>
#include <random>

#include "Scene.pb.h"

#include <google/protobuf/util/json_util.h>
#include <google/protobuf/message.h>

const std::string URL = "tcp://127.0.0.1:6576";

nng_socket sock;
int rv;

void sendSelection(const std::string& object)
{
  ROS_INFO_STREAM("Sending selection of: " << object);
  Selection selection;
  selection.set_id(object);
  int length = selection.ByteSize();
  void* data = nng_alloc(length);
  selection.SerializeToArray(data, length);

  if ((rv = nng_send(sock, data, length, NNG_FLAG_ALLOC)) != 0)
  {
    ROS_ERROR_STREAM("nng_send returned: " << nng_strerror(rv));
  }
}

int main(int argc, char** argv)
{
  GOOGLE_PROTOBUF_VERIFY_VERSION;

  ros::init(argc, argv, "dummy_selection_provider");

  ros::NodeHandle n("ccf");

  auto autoselect = CetiRosToolbox::getPrivateParameter<bool>("autoselect", "false");
  auto prefix = CetiRosToolbox::getPrivateParameter<std::string>("prefix", "sub_select");

  if ((rv = nng_pair1_open(&sock)) != 0)
  {
    ROS_ERROR_STREAM("nng_pair1_open returned: " << nng_strerror(rv));
  }

  ros::Rate connection_retry_rate(1);

  while ((rv = nng_dial(sock, URL.c_str(), nullptr, 0)) != 0)
  {
    ROS_WARN_STREAM("nng_dial returned: " << nng_strerror(rv) << ". Trying to connect again in one second...");
    connection_retry_rate.sleep();
  }
  ROS_INFO_STREAM("NNG connection established to " << URL);
  ros::Rate loop_rate(200);
  ros::Rate pause_rate(ros::Duration(2));  // seconds

  // initialize random number generator
  std::random_device rd;
  std::mt19937 rng(rd());

  ros::Subscriber sub_select =
      n.subscribe<std_msgs::String>(prefix + "/select", 1000, [&](auto& m) { sendSelection(m->data); });
  ros::Publisher pub_objects = n.advertise<std_msgs::String>(prefix + "/objects", 1);
  ros::Publisher pub_bins = n.advertise<std_msgs::String>(prefix + "/bins", 1);
  ros::Publisher pub_locations = n.advertise<std_msgs::String>(prefix + "/locations", 1);

  while (ros::ok())
  {
    char* buf = nullptr;
    size_t sz;
    if ((rv = nng_recv(sock, &buf, &sz, NNG_FLAG_ALLOC | NNG_FLAG_NONBLOCK)) == 0)
    {
      // store the result in a protobuf object and free the buffer
      Scene scene;
      scene.ParseFromArray(buf, sz);
      nng_free(buf, sz);

      std::string s;
      if (google::protobuf::TextFormat::PrintToString(scene, &s))
      {
        ROS_INFO_STREAM("Received a valid scene with " << scene.objects().size() << " objects.");
        ROS_DEBUG_STREAM("Content:" << std::endl << s);
      }
      else
      {
        ROS_WARN_STREAM("Received an invalid scene!" << std::endl
                                                     << "[dummy_selection_provider] Partial content:" << std::endl
                                                     << scene.ShortDebugString());
      }

      // collect all object names
      std::vector<std::string> objects{};
      std::vector<std::string> bins{};
      std::vector<std::string> locs{};
      for (const auto& object : scene.objects())
      {
        ROS_DEBUG_STREAM("found object " << object.id() << " of type " << Object_Type_Name(object.type()));
        if (object.type() == Object_Type_BOX)
        {
          objects.push_back(object.id());
        }
        else if (object.type() == Object_Type_BIN)
        {
          bins.push_back(object.id());
        }
        else if (object.type() == Object_Type_DROP_OFF_LOCATION)
        {
          locs.push_back(object.id());
        }
      }

      std_msgs::String objects_msg;
      objects_msg.data = objects.empty() ? "" :
                                           std::accumulate(++objects.begin(), objects.end(), *objects.begin(),
                                                           [](auto& a, auto& b) { return a + "," + b; });

      pub_objects.publish(objects_msg);

      std_msgs::String bins_msg;
      bins_msg.data = bins.empty() ? "" :
                                     std::accumulate(++bins.begin(), bins.end(), *bins.begin(),
                                                     [](auto& a, auto& b) { return a + "," + b; });

      pub_bins.publish(bins_msg);

      std_msgs::String locs_msg;
      bins_msg.data = locs.empty() ? "" :
                                     std::accumulate(++locs.begin(), locs.end(), *locs.begin(),
                                                     [](auto& a, auto& b) { return a + "," + b; });

      pub_locations.publish(locs_msg);

      if (autoselect)
      {
        if (objects.empty())
        {
          ROS_INFO("No objects to select in a scene without boxes!");
        }
        else if (bins.empty())
        {
          ROS_INFO("No bin to put the object in available!");
        }
        else
        {
          // select a random object
          std::uniform_int_distribution<u_long> distribution{ 0, objects.size() - 1 };

          // wait some time, then send a selected object
          pause_rate.sleep();
          std::string object{ objects[distribution(rng)] };

          ROS_INFO_STREAM("Selecting random object: " << object);
          sendSelection(object);

          // wait again, then send the bin object
          pause_rate.sleep();

          // we expect an object named "bin" to be there
          // select a random object
          std::uniform_int_distribution<u_long> binDistribution{ 0, bins.size() - 1 };

          std::string bin{ bins[binDistribution(rng)] };
          ROS_INFO_STREAM("Selecting random bin: " << bin);
          sendSelection(bin);

          // wait some time before starting the next selection
          pause_rate.sleep();
        }
      }
    }
    else if (rv == NNG_EAGAIN)
    {
      // no message received in current spin
    }
    else
    {
      ROS_ERROR_STREAM("nng_recv returned: " << nng_strerror(rv));
    }

    ros::spinOnce();

    loop_rate.sleep();
  }

  return 0;
}