#include "cell_controller.h"
std::optional<Action> CellController::triggerNextAction()
{
  if (current_action_ != nullptr && !current_action_->isCompleted(cell_.scene))
  {
    return std::nullopt;
  }
  // TODO this assumes each action can be performed only once, which holds for pick-and-drop tasks but not in general
  for (Action action : current_task_->getActions())
  {
    ROS_WARN_STREAM("Action completed: " << !action.isCompleted(cell_.scene));
    if (cell_.state != Cell::WORKING && !action.isCompleted(cell_.scene) && action.canBePerformed(cell_.scene))
    {
      cell_.state = Cell::WORKING;
      ROS_INFO_STREAM("Triggering action " << action);
      auto source = Action::lookupObject(cell_.scene, action.getSource());
      auto target = Action::lookupObject(cell_.scene, action.getTarget());
      if (!source || !target)
      {
        return std::nullopt;
      }
      current_action_ = std::make_shared<Action>(action.getType(), source->id(), target->id());
      action_callback_(*current_action_);
      return action;
    }
  }
  return std::nullopt;
}
bool CellController::currentActionCompleted()
{
  if (current_action_ == nullptr)
  {
    return true;
  }
  else if (current_action_->isCompleted(cell_.scene))
  {
    // the current action is completed, but this was not registered before, so we have to set the cell state
    cell_.state = Cell::IDLE;
    current_action_ = nullptr;
    return true;
  }
  else
  {
    return false;
  }
}
void CellController::updateModel(const std::string& scene_string)
{
  ROS_INFO_STREAM("Getting a scene update for cell " << cell_.id);

  cell_.scene.ParseFromString(scene_string);
  for (const auto& object : cell_.scene.objects())
  {
    if (object.type() == Object_Type_ARM)
    {
      cell_.state = object.state() == Object_State_STATE_IDLE ? Cell::IDLE : Cell::WORKING;
      break;
    }
  }
  proceedWithTask();
}
void CellController::setCurrentTask(const std::shared_ptr<Task>& new_task)
{
  if (!currentActionCompleted())
  {
    ROS_WARN_STREAM("Cell " << cell_.id
                            << " got a new task although an action of the previous task is still ongoing.");
  }
  current_task_.reset(new_task.get());
}
void CellController::proceedWithTask()
{
  if (currentActionCompleted())
  {
    auto action = triggerNextAction();
  }
}
