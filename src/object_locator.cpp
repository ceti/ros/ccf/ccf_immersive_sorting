/*! \file object_locator.cpp

    A ROS node that detects objects

    \author Johannes Mey
    \date 15.06.22
*/

#include <cmath>

#include <ros/ros.h>
#include <ros/package.h>
#include <apriltag_ros/AprilTagDetectionArray.h>

#include "std_msgs/String.h"

#include "tf2/LinearMath/Transform.h"

#include "ccf/util/NodeUtil.h"

#include "Scene.pb.h"

#include <google/protobuf/util/json_util.h>
#include <google/protobuf/message.h>
#include <ccf/controller/DummyRobotArmController.h>
#include <ccf/connection/MqttConnection.h>

using CetiRosToolbox::getParameter;
using CetiRosToolbox::getPrivateParameter;

const std::string CELL_BUNDLE = "CETI_TABLE_ONE";
const double TABLE_HEIGHT = 0.89;
const double GRID_SIZE = 0.05;
const double MAX_ERROR = 0.02;
const bool RISKY_MODE = true;
const bool SNAP_TO_GRID = true;

double distanceToGrid(geometry_msgs::Point point, double grid_size)
{
  double x_error = (std::round(point.x / grid_size)) * grid_size - point.x;
  double y_error = (std::round(point.y / grid_size)) * grid_size - point.y;
  return std::sqrt(x_error * x_error + y_error * y_error);
}

geometry_msgs::Point closestGridPoint(geometry_msgs::Point point, double grid_size)
{
  geometry_msgs::Point grid_point;
  grid_point.x = (std::round(point.x / grid_size)) * grid_size;
  grid_point.y = (std::round(point.y / grid_size)) * grid_size;
  grid_point.z = point.z;
  return grid_point;
}

int main(int argc, char** argv)
{
  GOOGLE_PROTOBUF_VERIFY_VERSION;

  ros::init(argc, argv, "object_locator");

  ros::NodeHandle n("ccf");

  ros::Rate loop_rate(200);

  ros::Duration(2).sleep(); // sleep for two seconds

  DummyRobotArmController controller(n, "object_locator", "nobot");

  auto mqttServer = getPrivateParameter<std::string>("mqtt_server", "localhost:1883");
  std::shared_ptr<MqttConnection> mqtt_connection =
      std::make_unique<MqttConnection>(mqttServer, ros::this_node::getName());
  auto observed_scene_listener_topic =
      getPrivateParameter<std::string>("other_cell_listener", "/ceti_cell_1/scene/update");
  auto observed_scene_delta_update_topic =
      getPrivateParameter<std::string>("other_cell_delta_updates", "object_locator/scene/update");
  controller.addConnection(mqtt_connection);
  mqtt_connection->listen(observed_scene_listener_topic);

  // subscribe to a scene
  controller.addCallback(observed_scene_listener_topic, [&controller](auto msg) {
    ROS_INFO_STREAM("Updating scene from other cell.");
    Scene scene;
    scene.ParseFromString(msg);
    controller.initScene(scene, false);
  });

  controller.initScene({}, false);

  Scene objectLibrary = RobotArmController::loadSceneFromFile(
      getPrivateParameter<std::string>("object_library", ros::package::getPath("ccf_immersive_sorting") + "/config/"
                                                                                                          "objects."
                                                                                                          "json"));

  ROS_DEBUG_STREAM("object library contains " << objectLibrary.objects_size() << " objects");

  std::map<std::string, geometry_msgs::Pose> located_poses;

  // read the bundles and extract the base pose
  std::map<int, std::string> tagBundleTags;
  auto tagBundles = CetiRosToolbox::getParameter<XmlRpc::XmlRpcValue>(n, "/apriltag_ros_continuous_node/tag_bundles");
  for (int bundle_index = 0; bundle_index < tagBundles.size(); bundle_index++)
  {
    auto& bundle = tagBundles[bundle_index];
    for (int tag_index = 0; tag_index < bundle["layout"].size(); tag_index++)
    {
      auto& tag = bundle["layout"][tag_index];
      tagBundleTags[tag["id"]] = std::string(bundle["name"]);
    }
  }

  ros::Subscriber sub_tages = n.subscribe<apriltag_ros::AprilTagDetectionArray>(
      getPrivateParameter<std::string>("tag_topic", "/tag_detections"), 1000, [&](auto& m) {
        tf2::Transform base_pose;  // pose of the origin relative to the camera
        std::map<std::string, geometry_msgs::Pose> allObjectPoses;

        // check if the base frame was detected
        bool base_tag_found = false;
        for (const auto& detection : m->detections)
        {
          if (tagBundleTags[detection.id[0]] == CELL_BUNDLE)
          {  // a base tag was found if an id of the cell bundle was found
            base_tag_found = true;
            base_pose.setOrigin({ detection.pose.pose.pose.position.x, detection.pose.pose.pose.position.y,
                                  detection.pose.pose.pose.position.z });
            base_pose.setRotation({ detection.pose.pose.pose.orientation.x, detection.pose.pose.pose.orientation.y,
                                    detection.pose.pose.pose.orientation.z, detection.pose.pose.pose.orientation.w });
          }
          else
          {
            allObjectPoses[tagBundleTags[detection.id[0]]] = detection.pose.pose.pose;
          }
        }
        if (!base_tag_found)
        {
          ROS_WARN_STREAM("Skipping tag detection because no base tag was found");
          return;
        }

        // make pose relative to origin instead of camera
        for (auto& [name, pose] : allObjectPoses)
        {
          tf2::Transform object({ pose.orientation.x, pose.orientation.y, pose.orientation.z, pose.orientation.w },
                                { pose.position.x, pose.position.y, pose.position.z });
          tf2::Transform result = base_pose.inverseTimes(object);
          pose.position.x = result.getOrigin().x();
          pose.position.y = result.getOrigin().y();
          pose.position.z = result.getOrigin().z();
          pose.orientation.x = result.getRotation().x();
          pose.orientation.y = result.getRotation().y();
          pose.orientation.z = result.getRotation().z();
          pose.orientation.w = result.getRotation().w();
        }

        // create a delta scene to send later (if not empty)
        Scene updateScene;
        updateScene.set_is_delta(true);

        // filter object poses
        std::map<std::string, geometry_msgs::Pose> relevant_poses;
        for (const auto& [name, pose] : allObjectPoses)
        {
          tf2::Quaternion q{ pose.orientation.x, pose.orientation.y, pose.orientation.z, pose.orientation.w };
          std::cout.precision(3);

          auto existingObject = controller.resolveObject(name);  // we need this also later to check if pose has changed
          if (!RISKY_MODE && existingObject)
          {
            ROS_DEBUG_STREAM("skipping pose for " << name << " the object is already in the scene.");
            continue;
          }

          if (pose.orientation.w < 0.99)
          {
            ROS_WARN_STREAM("skipping pose for " << name << " because the object is rotated " << pose.orientation << " "
                                                 << q.getAxis() << " " << q.angleShortestPath({ 0, 0, 0, 1 }));
            continue;
          }
          if (std::abs(pose.position.z - TABLE_HEIGHT) > 0.02)
          {
            ROS_WARN_STREAM("skipping pose for " << name << " because the object is not on table level "
                                                 << pose.position.z - TABLE_HEIGHT);
            continue;
          }
          double e = distanceToGrid(pose.position, GRID_SIZE);
          ROS_DEBUG_STREAM("object " << name << " has an error of " << e);

          // sanitize pose
          if (SNAP_TO_GRID)
          {
            relevant_poses[name] = pose;
            relevant_poses[name].position.z = TABLE_HEIGHT;
            relevant_poses[name].position = closestGridPoint(relevant_poses[name].position, GRID_SIZE);
            relevant_poses[name].orientation.x = 0;
            relevant_poses[name].orientation.y = 0;
            relevant_poses[name].orientation.z = 0;
            relevant_poses[name].orientation.w = 1;

            if (RISKY_MODE && existingObject)
            {
              double dx = existingObject->pos().x() - relevant_poses[name].position.x;
              double dy = existingObject->pos().y() - relevant_poses[name].position.y;
              double distance = std::sqrt(dx * dx + dy * dy);
              if (distance < GRID_SIZE / 2)
              {
                ROS_DEBUG_STREAM("Skipping object  " << name << " because its location has not changed.");
                continue;
              }
              else
              {
                ROS_DEBUG_STREAM("Sending object  " << name << " because its location has changed by " << distance);
              }
            }
          }

          if (RISKY_MODE || located_poses[name] == relevant_poses[name])
          {
            if (RISKY_MODE)
            {
              ROS_DEBUG_STREAM("found the new pose for " + name + ": " << relevant_poses[name].position.x << ", "
                                                                       << relevant_poses[name].position.y);
            }
            else
            {
              ROS_DEBUG_STREAM("found the same pose for " + name + " two times in a row: "
                               << relevant_poses[name].position.x << ", " << relevant_poses[name].position.y);
            }
            auto object = DummyRobotArmController::resolveObject(name, objectLibrary);
            if (!object)
            {
              ROS_WARN_STREAM("skipping pose for " << name << " because the object is not in the object library.");
              continue;
            }
            auto newObject = updateScene.add_objects();
            *newObject = *object;
            newObject->mutable_pos()->set_x(located_poses[name].position.x);
            newObject->mutable_pos()->set_y(located_poses[name].position.y);
            newObject->mutable_pos()->set_z(located_poses[name].position.z + newObject->size().height() / 2);
            newObject->set_mode(existingObject ? Object_Mode_MODE_MODIFY : Object_Mode_MODE_ADD);
            newObject->set_state(Object_State_STATE_STATIONARY);
          }
        }

        if (updateScene.objects_size() > 0)
        {
          ROS_DEBUG_STREAM("Sending scene with " << updateScene.objects_size() << " newly recognized or updated objects.");
          controller.sendToAll(observed_scene_delta_update_topic, updateScene.SerializeAsString());
        }
        located_poses = relevant_poses;
      });

  while (ros::ok())
  {
    ros::spinOnce();
    loop_rate.sleep();
  }

  return 0;
}
