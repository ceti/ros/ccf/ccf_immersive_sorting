#include "action.h"

std::ostream& operator<<(std::ostream& os, const Action& action)
{
  switch (action.type_)
  {
    case Action::PICK_DROP:
      os << "[pick " << action.source_ << " and drop it into " << action.target_ << "]";
      break;
    default:
      os << "[unknown action with source " << action.source_ << " and target " << action.target_ << "]";
  }
  return os;
}
std::optional<Object> Action::lookupObject(const Scene& scene, const std::string& id)
{
  for (Object object : scene.objects())
  {
    // if the id can be found at position 0
    if (object.id().rfind(id, 0) == 0)
    {
      return object;
    }
  }
  return std::nullopt;
}
Action::Action(Action::Type type, std::string source, std::string target)
    : type_(type), source_(std::move(source)), target_(std::move(target))
{
}
bool Action::canBePerformed(const Scene& scene)
{
  auto s = lookupObject(scene, source_);
  auto t = lookupObject(scene, target_);
  return s && t && s->type() == requiredSourceType(type_) && t->type() == requiredTargetType(type_);
}
bool Action::isCompleted(const Scene& scene) const
{
  switch (type_)
  {
    case Action::Type::PICK_DROP:
      // pick and drop is completed if the dropped element no longer exists
      return !lookupObject(scene, source_).has_value();
    case Action::Type::UNDEFINED:
      // undefined actions are defined to be completed
    default:
      return true;
  }
}
