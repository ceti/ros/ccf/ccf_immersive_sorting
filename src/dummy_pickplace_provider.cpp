/*! \file dummy_pickplace_provider.cpp

    A ROS node that can trigger robot commands in a cell

    \author Johannes Mey
    \date 26.04.2022
*/

#include "ros/ros.h"
#include "std_msgs/String.h"

#include "ccf/util/NodeUtil.h"

#include "Scene.pb.h"

#include <google/protobuf/message.h>

#include "ccf/connection/MqttConnection.h"

using CetiRosToolbox::getPrivateParameter;

int main(int argc, char** argv)
{
  GOOGLE_PROTOBUF_VERIFY_VERSION;

  ros::init(argc, argv, "dummy_pickplace_provider");

  ros::NodeHandle n("ccf");

  auto mqtt_server = getPrivateParameter<std::string>("mqtt_server", "tcp://127.0.0.1:1883");
  auto prefix = CetiRosToolbox::getPrivateParameter<std::string>("prefix", "sub_select");
  auto target_cell = CetiRosToolbox::getPrivateParameter<std::string>("target_cell", "ceti_cell_placeworld");

  std::shared_ptr<MqttConnection> client_connection =
      std::make_shared<MqttConnection>(mqtt_server, ros::this_node::getName());

  client_connection->initializeConnection(
      [](auto t, auto m) { ROS_WARN_STREAM("ignoring message " << m << " on topic " << t); });

  ros::Subscriber sub_select = n.subscribe<std_msgs::String>(prefix + "/pickplace", 1000, [&](auto& m) {
    std::string command = m->data;

    Command c;
    size_t pos = command.find(',');
    c.mutable_pickandplace()->set_idpick(command.substr(0, pos));
    command.erase(0, pos + 1);
    pos = command.find(',');
    c.mutable_pickandplace()->set_idplace(command.substr(0, pos));
    command.erase(0, pos + 1);
    c.mutable_pickandplace()->set_idrobot(command);

    ROS_INFO_STREAM("sending command for robot " << c.pickandplace().idrobot() << " to place "
                                                 << c.pickandplace().idpick() << " at " << c.pickandplace().idplace());
    client_connection->send("/" + target_cell + "/command", c.SerializeAsString());
  });

  ros::Subscriber sub_evacuate = n.subscribe<std_msgs::String>(prefix + "/evacuate", 1000, [&](auto& m) {
    std::string command = m->data;

    Command c;
    size_t pos = command.find(',');
    c.mutable_evacuate()->set_idcollaborationzone(command.substr(0, pos));
    command.erase(0, pos + 1);
    c.mutable_evacuate()->set_idrobot(command);

    ROS_INFO_STREAM("sending command for robot " << c.evacuate().idrobot() << " to evacuate "
                                                 << c.evacuate().idcollaborationzone());
    client_connection->send("/" + target_cell + "/command", c.SerializeAsString());
  });

  ros::Subscriber sub_change_zone = n.subscribe<std_msgs::String>(prefix + "/configzone", 1000, [&](auto& m) {
    std::string command = m->data;

    Command c;
    size_t pos = command.find(',');
    c.mutable_configchange()->set_idcollaborationzone(command.substr(0, pos));
    command.erase(0, pos + 1);
    c.mutable_configchange()->set_idrobotnewowner(command);

    ROS_INFO_STREAM("sending command to assign robot " << c.configchange().idrobotnewowner() << " to zone "
                                                       << c.configchange().idcollaborationzone());
    client_connection->send("/" + target_cell + "/command", c.SerializeAsString());
  });

  ROS_INFO_STREAM("ROBOT COMMAND PROVIDER" << std::endl
                                           << "To trigger a command, use" << std::endl
                                           << "  rostopic pub -1 /ccf/" << prefix
                                           << "/pickplace std_msgs/String \"<object>,<target>,<robot>\"" << std::endl
                                           << "  rostopic pub -1 /ccf/" << prefix
                                           << "/evacuate std_msgs/String \"<collaboration_zone>,<robot>\"" << std::endl
                                           << "  rostopic pub -1 /ccf/" << prefix
                                           << "/configzone std_msgs/String \"<collaboration_zone>,<robot>\"");

  ros::spin();

  return 0;
}