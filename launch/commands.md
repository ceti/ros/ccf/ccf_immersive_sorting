# Useful commands

This document is a cheat sheet for useful commands

## Image detection

- figure out the correct camera and its settings
    - get the correct device
      `v4l2-ctl --list-devices`
    - show device information (change device depending on output of above command)
      `v4l2-ctl -d /dev/video2 --all`
- show (rectified) image of camera
  `rosrun image_view image_view image:=/usb_cam/image_rect`
- show detected image
  `rosrun image_view image_view image:=/tag_detections_image`
- interpret the apriltag message:
  `rosmsg show apriltag_ros/AprilTagDetectionArray`
  ```
  std_msgs/Header header
    uint32 seq
    time stamp
    string frame_id
  apriltag_ros/AprilTagDetection[] detections
    int32[] id
    float64[] size
    geometry_msgs/PoseWithCovarianceStamped pose
      std_msgs/Header header
        uint32 seq
        time stamp
        string frame_id
      geometry_msgs/PoseWithCovariance pose
        geometry_msgs/Pose pose
          geometry_msgs/Point position
            float64 x
            float64 y
            float64 z
          geometry_msgs/Quaternion orientation
            float64 x
            float64 y
            float64 z
            float64 w
        float64[36] covariance
  ```

## TF and RVIZ

- open a new RVIZ to show transformations
  `rosrun rviz rviz`
    - set **Fixed Frame** to `CETI_TABLE_ONE`
    - select Add -> TF to show TF objects

## Robot Controllers

- send a scene manually
  `rostopic pub -1 /connector_node_ros_ccf/send_scene std_msgs/Empty`
