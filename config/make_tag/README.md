# make_tag

taken
from [https://github.com/berndpfrommer/tagslam](https://github.com/berndpfrommer/tagslam/blob/master/src/make_tag.py)
under Apache 2.0 license.

base tags (0-3):

```
python make_tag.py --draw_box --nx 2 --ny 2 --no-symm_corners --tsize 0.08 --borderbits 1 --startid 0 --tspace 0.03 --tfam t36h11 tags.pdf
```

large tags (4-27):

```
python make_tag.py --draw_box --nx 3 --ny 4 --no-symm_corners --tsize 0.052 --borderbits 1 --startid 4 --tspace 0.03 --tfam t36h11 tags.pdf
python make_tag.py --draw_box --nx 3 --ny 4 --no-symm_corners --tsize 0.052 --borderbits 1 --startid 16 --tspace 0.03 --tfam t36h11 tags.pdf
```

small tags (28-75):

```
python make_tag.py --draw_box --nx 6 --ny 8 --no-symm_corners --tsize 0.024 --borderbits 1 --startid 28 --tspace 0.06 --tfam t36h11 tags.pdf
```