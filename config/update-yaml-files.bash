#!/bin/bash

# (re-)generate json configs from yaml files.
# This command requires yq, which can be installed in ubuntu using `snap install yq`.

for FILENAME in config_scene*.yaml objects.yaml; do
    yq eval -o=json $FILENAME > "${FILENAME%%.*}.json"
done