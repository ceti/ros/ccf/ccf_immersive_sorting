#ifndef CCF_IMMERSIVE_SORTING_ACTION_H_
#define CCF_IMMERSIVE_SORTING_ACTION_H_

#include <string>

#include "Scene.pb.h"

class Action
{
public:
  enum Type
  {
    UNDEFINED,
    PICK_DROP
  };

private:
  const Type type_;
  const std::string source_;
  const std::string target_;

public:
  bool operator==(const Action& rhs) const
  {
    return type_ == rhs.type_ && source_ == rhs.source_ && target_ == rhs.target_;
  }
  bool operator!=(const Action& rhs) const
  {
    return !(rhs == *this);
  }

private:
  static constexpr Object::Type requiredSourceType(Type type)
  {
    switch (type)
    {
      case PICK_DROP:
        return Object::BOX;
      case UNDEFINED:
      default:
        return Object::UNKNOWN;
    }
  }
  static constexpr Object::Type requiredTargetType(Type type)
  {
    switch (type)
    {
      case PICK_DROP:
        return Object::BIN;
      case UNDEFINED:
      default:
        return Object::UNKNOWN;
    }
  }

public:
  /// Lookup for an object starting with a prefix
  /// \param scene
  /// \param id the prefix the object starts with
  /// \return
  [[nodiscard]] static std::optional<Object> lookupObject(const Scene& scene, const std::string& id);

  [[nodiscard]] Type getType() const
  {
    return type_;
  }

  [[nodiscard]] const std::string& getSource() const
  {
    return source_;
  }

  [[nodiscard]] const std::string& getTarget() const
  {
    return target_;
  }

public:
  /**
   * Constructor
   * @param type
   * @param source
   * @param target
   */
  Action(Type type, std::string source, std::string target);

  /**
   * an action can be performed in a scene if source and target exist in the scene and have the correct type for the
   * action
   * @param scene
   * @return true if the action can be performed
   */
  bool canBePerformed(const Scene& scene);

  [[nodiscard]] bool isCompleted(const Scene& scene) const;

  friend std::ostream& operator<<(std::ostream& os, const Action& action);
};

std::ostream& operator<<(std::ostream& os, const Action& action);

#endif  // CCF_IMMERSIVE_SORTING_ACTION_H_
