#ifndef CCF_IMMERSIVE_SORTING_TASK_H_
#define CCF_IMMERSIVE_SORTING_TASK_H_

#include <vector>
#include "action.h"

class Task
{
  std::vector<Action> actions_;

public:
  void addAction(const Action& action)
  {
    actions_.emplace_back(action);
  }

  [[nodiscard]] const std::vector<Action>& getActions() const
  {
    return actions_;
  }
};

#endif  // CCF_IMMERSIVE_SORTING_TASK_H_
