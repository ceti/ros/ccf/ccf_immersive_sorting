#ifndef CCF_IMMERSIVE_SORTING_CELL_CONTROLLER_H_
#define CCF_IMMERSIVE_SORTING_CELL_CONTROLLER_H_

#include <utility>

#include "cell.h"
#include "action.h"
#include "task.h"

#include "ros/ros.h"

class CellController
{
  Cell cell_;
  std::shared_ptr<Action> current_action_;
  std::shared_ptr<Task> current_task_;
  std::function<void(const Action&)> action_callback_;

public:
  CellController()
  {
    cell_.state = Cell::DISCONNECTED;
  }

  void setCellId(const std::string& id)
  {
    cell_.id = id;
  }

  void setActionCallback(const std::function<void(const Action&)>& action_callback)
  {
    action_callback_ = action_callback;
  }

  const Cell& cell() const
  {
    return cell_;
  }

  [[maybe_unused]] bool hasCurrentAction() const
  {
    return current_action_ == nullptr;
  }

  [[maybe_unused]] const Action& getCurrentAction() const
  {
    return *current_action_;
  }

  [[maybe_unused]] const std::shared_ptr<Task>& getCurrentTask() const
  {
    return current_task_;
  }

  void setCurrentTask(const std::shared_ptr<Task>& new_task);

  void proceedWithTask();

  void updateModel(const std::string& scene_string);

  /**
   * A current action is completed if there is no current action or the current action can no longer be performed
   * @return
   */
  bool currentActionCompleted();

  std::optional<Action> triggerNextAction();
};

#endif  // CCF_IMMERSIVE_SORTING_CELL_CONTROLLER_H_
