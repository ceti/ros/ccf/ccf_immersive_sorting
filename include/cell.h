#ifndef CCF_IMMERSIVE_SORTING_CELL_H_
#define CCF_IMMERSIVE_SORTING_CELL_H_

#include <Scene.pb.h>
#include <optional>
#include <string>

struct Cell
{
  enum State
  {
    UNDEFINED,
    DISCONNECTED,
    IDLE,
    WORKING
  };

  std::string stateString() const
  {
    switch (state)
    {
      case DISCONNECTED:
        return "DISCONNECTED";
      case IDLE:
        return "IDLE";
      case WORKING:
        return "WORKING";
      default:
        return "UNDEFINED";
    }
  }

  std::string id;
  Scene scene;
  State state = UNDEFINED;

  Cell() = default;

  std::string robot() const
  {
    for (const auto& object : scene.objects())
    {
      if (object.type() == Object_Type_ARM)
      {
        return object.id();
      }
    }
    return "<no robot found>";
  }

  // one does not copy cells
  Cell(const Cell&) = delete;
  Cell& operator=(const Cell&) = delete;
};

#endif  // CCF_IMMERSIVE_SORTING_CELL_H_
